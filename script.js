// Опишіть своїми словами що таке Document Object Model (DOM)
// DOM – це об'єктна модель документа (Document Object Model).
// Завантажується в браузер у вигляді деревоподібної структури з HTML коду
// Структура складається з вузлів, вкладених один одного

// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerText - показує весь текстовий вміст, який не відноситься до синтаксису HTML.
// Тобто будь-який текст, укладений між тегами елемента, що відкривають і закривають, буде записаний в innerText.
// Причому якщо всередині innerText будуть ще якісь елементи HTML зі своїм вмістом,
// він проігнорує самі елементи і поверне їх внутрішній текст.
// innerHTML - Покаже текстову інформацію рівно по одному елементу.
// У висновок потрапить і текст і розмітка HTML-документа, між тегами основного елемента,
// що відкриваються і закриваються.

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// За допомогою пошукових методів:
// getElementById() - звернення до елементу за унікальнім id;
// getElementsByName() - пошук серед елементів з атрибутом name, повертає список всіх елементів,чий атрибут name задовольняє запиту;
// getElementsByClassName() - звернення до всіх елементів з однаковою назвою классу;
// getElementsByTagName() - звернення до всіх елементів з однаковою назвою тегу;
// querySelector() - шукає та повертає перший елемент, що задовольняє даному CSS-селектору;
// querySelectorAll() - повертає всі елементи, що задовольняють даномму CSS-селектору.
// Частіше використовуються методи querySelector() та querySelectorAll(). 

// task1
// Знайти всі параграфи на сторінці (вивела у консоль) та встановити колір фону #ff0000

// let elements = document.querySelectorAll('p');
// console.log(elements);

// for(let a of elements) {
// 	a.style.backgroundColor = "#ff0000";
// }


//  task2
//  Знайти елемент із id="optionsList". Вивести у консоль.
//  Знайти батьківський елемент та вивести в консоль.
//  Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// let elem = document.getElementById('optionsList');
// console.log(elem);
// console.log(elem.parentElement);

// let nodes = elem.childNodes;
// for (let node of nodes) {
// 	console.log(node.nodeName, node.nodeType);
// };


//  task3
// Встановіть в якості контента елемента з класом testParagraph наступний параграф -This is a paragraph.
// Дз8 в пункті 3, замість параграфа шукайте по id.

// let elem = document.getElementById('testParagraph');
// elem.innerText = 'This is a paragraph';

// or
// elem.textContent = 'This is a paragraph';


// console.log(elem);


//  task4
// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.

let elements = document.querySelector(".main-header").children;
console.log(elements);

for ( let elem of elements) { 
	elem.classList.add("nav-item");
 };
 console.log(elements);

//  or

// elements.forEach((elem)=> {
// 		elem.classList.add("nav-item");
// 	});
//  console.log(elem);


//  task5
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

// let elements = document.querySelectorAll(".section-title");
// console.log(elements);

// elements.forEach ((elem)=> {
// 	elem.classList.remove("section-title");
// });
// console.log(elements);

//  or

// for ( let elem of elements) { 
// 	elem.classList.remove("section-title");
//  };
// console.log(elements);
